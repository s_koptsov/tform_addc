Write-Output "ConfigureDC.ps1 beginning"

# Install the nuget provider
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force

# Trust the PSGallery repo
Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted

# Install the required DSC modules
Install-Module xActiveDirectory,xComputerManagement,xNetworking,xAdcsDeployment,xDnsServer,xSystemSecurity,Pester -force

# Download and execute the configuration script
Invoke-WebRequest -Uri https://bitbucket.org/s_koptsov/tform_addc/raw/b4e609e0971bf75f83bd77ef6ff01259d2339ec2/DSC-DC.ps1 -UseBasicParsing | Invoke-Expression



